#!/usr/bin/env python

import argparse
import requests
import json
import jwt

parser = argparse.ArgumentParser(description='Hammerhead Dashboard Sync')

parser.add_argument('-u', '--username', required=True)
parser.add_argument('-p', '--password', required=True)

args = parser.parse_args()

base_url = 'https://dashboard.hammerhead.io/v1'

def getToken(username, password):
  payload = {
    'grant_type': 'password',
    'username': username,
    'password': password,
  }

  headers = {"Content-Type":"application/x-www-form-urlencoded"}

  try:
    r = requests.post(base_url + '/auth/token', data=payload, headers=headers)
  except requests.exceptions.RequestException as e:
    print(e)
    sys.exit(1)

  response = r.json()
  token = response['access_token']

  return token

def getUserID(token):
  decoded = jwt.decode(token, audience="hhApp", algorithms=["HS256"], options={"verify_signature": False})
  user_id = decoded['sub']

  return user_id

def syncRoutes(token, userid):
  endpoint = base_url + '/users/' + userid + '/routes/sync'
  headers = headers = {"Authorization":"Bearer " + token + ""}

  try:
    r = requests.post(endpoint, data=None, headers=headers)
    if r.ok:
      print("Successful sync! Let's Ride!")
    else:
      print("Oops! Something went wrong")
  except requests.exceptions.RequestException as e:
    print(e)
    sys.exit(1)

def main():
  token = getToken(args.username, args.password)
  user_id = getUserID(token)
  syncRoutes(token, user_id)

if __name__=="__main__":
  main()
