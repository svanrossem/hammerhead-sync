# hammerhead-sync

I own a Hammerhead Karoo 2 (https://www.eu.hammerhead.io/products/hammerhead-karoo-2) cycling GPS-device. It's a great device with a lot of options, but it's lacking one feature. After creating a route on Strava or marking a friends ride as favourite, you need to manually hit the synchronisation button in Hammerhead's online dashboard (https://dashboard.hammerhead.io/)...
Really annoying, until now!

## Getting started

1. Install dependencies system-wide or using Python virtual environment (https://docs.python.org/3/tutorial/venv.html)

    `pip install -r requirements.txt`

2. Run python script with correct arguments

    `python hammerhead-route-sync.py --username john.doe@example.com --password thisisasecret`

3. Set above as a cronjob or whatever you like
